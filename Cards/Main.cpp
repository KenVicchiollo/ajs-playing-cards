// Playing Cards
// Part 1 : AJ Vetter

// Part 2 : Ken Vicchiollo

#include <iostream>;
#include <conio.h>;
#include <string>;

using namespace std;

enum CardSuit { Diamonds, Clubs, Hearts, Spades };
enum CardRank {	Two = 2, Three,	Four, Five,	Six, Seven, Eight, Nine, Ten, Jack,	Queen, King, Ace };

struct Card
{
	CardSuit Suit;
	CardRank Rank;
};

// Function Prototypes
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);


int main() 
{
	Card testCard1;
	testCard1.Rank = Queen;
	testCard1.Suit = Diamonds;

	Card testCard2;
	testCard2.Rank = Three;
	testCard2.Suit = Spades;

	PrintCard(HighCard(testCard1, testCard2));

	(void)_getch();
	return 0;
}

// Prints the rank and suit of a card
void PrintCard(Card card) 
{
	string cardRank;
	switch (card.Rank)
	{
	case Two: 
		cardRank = "Two";
		break;
	case Three: 
		cardRank = "Three";
		break;
	case Four:
		cardRank = "Four";
		break;
	case Five:
		cardRank = "Five";
		break;
	case Six:
		cardRank = "Six";
		break;
	case Seven:
		cardRank = "Seven";
		break;
	case Eight:
		cardRank = "Eight";
		break;
	case Nine:
		cardRank = "Nine";
		break;
	case Ten:
		cardRank = "Ten";
		break;
	case Jack:
		cardRank = "Jack";
		break;
	case Queen:
		cardRank = "Queen";
		break;
	case King:
		cardRank = "King";
		break;
	case Ace:
		cardRank = "Ace";
		break;
	}

	string cardSuit;
	switch (card.Suit)
	{
	case Diamonds: 
		cardSuit = "Diamonds";
		break;
	case Clubs:
		cardSuit = "Clubs";
		break;
	case Hearts:
		cardSuit = "Hearts";
		break;
	case Spades:
		cardSuit = "Spades";
		break;
	}

	cout << "The " << cardRank << " of " << cardSuit << "\n";
}

/* Compares two cards.  Returns the card with the highest rank.  
   If cards have equal rank, the first card is returned. */
Card HighCard(Card card1, Card card2)
{
	if (card1.Rank >= card2.Rank) 
	{
		return card1;
	}
	else
	{
		return card2;
	}
}